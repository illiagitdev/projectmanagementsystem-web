<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>About</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="about_content">
    <h2>About project management system</h2>
        <h3>WEB application</h3>
        <h4>Features</h4>
        <pre>
        1. Developers
        2. Companies
        3. Projects
        4. Clients
        </pre>
        <h4>Technologies</h4>
        <pre>
        - Java Core
        - Gradle
        - Hibernate, ORM
        - PostgreSQL
        - MVC, DAO, DTO
        - Servlet, JSP
        </pre>
</div>
</body>
</html>