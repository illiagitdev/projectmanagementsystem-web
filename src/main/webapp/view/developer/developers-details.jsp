<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Developer</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
    <c:choose>
    <c:when test="${not empty developer}">
    <h1>Developer details</h1>
    <table class="zui-table">
        <tbody>
        <tr>
            <td>First name:</td>
            <td>${developer.firstName}</td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td>${developer.lastName}</td>
        </tr>
        <tr>
            <td>Age:</td>
            <td>${developer.age}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>${developer.email}</td>
        </tr>
        <tr>
            <td>Salary:</td>
            <td>${developer.salary}</td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/developer/developerUpdate?id=${developer.id}" class="button" role="button" tabindex="0">Update</a>
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/developer/developerDelete?id=${developer.id}" class="button" role="button" tabindex="0">Delete</a>
            </td>
        </tr>
        </tbody>
    </table>
    </c:when>
    <c:otherwise>
    <h1>Developers details not available at the moment</h1>
    </c:otherwise>
    </c:choose>
    <div/>
</body>
</html>