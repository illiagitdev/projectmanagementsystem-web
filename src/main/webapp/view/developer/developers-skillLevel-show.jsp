<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Developers</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="get" action="showBySkillLevelDevelopers">
        <h1>Select skill level to filter developers:</h1>
        <label>Skill level
            <span class="small">Select</span>
        </label>
        <select name="skillLevel" id="skillLevel">
            <c:forEach items="${skillLevelList}" var="levels">
                <option>${levels}</option>
            </c:forEach>
        </select>
        <button type="submit">Show</button>
        <div class="spacer"></div>
    </form>

    <c:if test="${not empty developers}">
        <h1>${message}</h1>

        <table class="zui-table">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${developers}" var="developer">
                <tr>
                    <td><c:out value="${developer.firstName}"/></td>
                    <td><c:out value="${developer.lastName}"/></td>
                    <td><c:out value="${developer.email}"/></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/developer/developerDetails?id=${developer.id}" class="button" role="button" tabindex="0">Details</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </c:if>
</div>
</body>
</html>