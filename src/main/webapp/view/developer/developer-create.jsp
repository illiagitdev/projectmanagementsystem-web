<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Developer</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createDeveloper">
        <h1>Create developer form</h1>

        <label>First Name
            <span class="small">Enter first name</span>
        </label>
        <input type="text" name="firstName" id="firstName" />

        <label>Last Name
            <span class="small">Enter last name</span>
        </label>
        <input type="text" name="lastName" id="lastName" />

        <label>Age
            <span class="small">Add age</span>
        </label>
        <input type="number" name="age" id="age" />

        <label>Email
            <span class="small">Enter email</span>
        </label>
        <input type="text" name="email" id="email" />

        <label>Select gender
            <span class="small">Select gender</span>
        </label>
        <select name="gender" id="gender">
            <c:forEach items="${gender}" var="sex">
                <option>${sex}</option>
            </c:forEach>
        </select>

        <label>Set salary
            <span class="small">Set salary</span>
        </label>
        <input type="number" name="salary" id="salary" />

        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>

</div>
</body>
</html>