<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update Developer</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="developerUpdate">
        <h1>Update developer form</h1>

        <input hidden  name="id" id="id" value="${developer.id}"/>

        <label>First Name
            <span class="small">Enter first name</span>
        </label>
        <input type="text" name="firstName" id="firstName" value="${developer.firstName}" />

        <label>Last Name
            <span class="small">Enter last name</span>
        </label>
        <input type="text" name="lastName" id="lastName" value="${developer.lastName}" />

        <label>Age
            <span class="small">Set age</span>
        </label>
        <input type="number" name="age" id="age" value="${developer.age}" />

        <label>Email
            <span class="small">Enter email</span>
        </label>
        <input type="text" name="email" id="email" value="${developer.email}" />

        <label style="color: red">Select gender
            <span class="small">Select gender</span>
        </label>
        <select name="gender" id="gender">
            <c:forEach items="${gender}" var="sex">
                <option>${sex}</option>
            </c:forEach>
        </select>

        <label>Update salary
            <span class="small">Salary</span>
        </label>
        <input type="number" name="salary" id="salary" value="${developer.salary}"/>

        <button type="submit">Update</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>