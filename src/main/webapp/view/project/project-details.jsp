<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Project</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
    <c:choose>
    <c:when test="${not empty project}">
    <h1>Project details</h1>
    <table class="zui-table">
        <tbody>
        <tr>
            <td>Project name:</td>
            <td>${project.name}</td>
        </tr>
        <tr>
            <td>Project budget:</td>
            <td>${project.cost}</td>
        </tr>
        <tr>
            <td>Project start date:</td>
            <td>${project.projectStart}</td>
        </tr>
        <tr>
            <td>Project release date:</td>
            <td>${project.releaseDate}</td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/project/projectUpdate?id=${project.id}" class="button" role="button" tabindex="0">Update</a>
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/project/projectDelete?id=${project.id}" class="button" role="button" tabindex="0">Delete</a>
            </td>
        </tr>
        </tbody>
    </table>
    </c:when>
    <c:otherwise>
    <h1>Project details not available at the moment</h1>
    </c:otherwise>
    </c:choose>
    <div/>
</body>
</html>