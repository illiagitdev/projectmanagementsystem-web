<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Expenses on developers</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
    <h3>Expenses on developers by project</h3>
    <c:if test="${(not empty expensesMap)}">

        <table class="zui-table">
            <thead>
            <tr>
                <th>Project Name</th>
                <th>Total salary</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${expensesMap}" var="map">
                <tr>
                    <td><c:out value="${map.key}"/></td>
                    <td><c:out value="${map.value}"/></td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </c:if>
</div>
</body>
</html>