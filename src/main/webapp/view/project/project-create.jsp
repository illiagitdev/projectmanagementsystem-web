<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Project</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createProject">
        <h1>Create project form</h1>

        <label>Name
            <span class="small">Add name</span>
        </label>
        <input type="text" name="name" id="name" />


        <label>Project start date
            <span class="small">Project start date</span>
        </label>
        <input type="date" name="projectStart" id="projectStart" />


        <label>Project cost
            <span class="small">Add project cost</span>
        </label>
        <input type="number" name="cost" id="cost" />


        <label>Release date
            <span class="small">Add release date</span>
        </label>
        <input type="date" name="releaseDate" id="releaseDate" />


        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>

</div>
</body>
</html>