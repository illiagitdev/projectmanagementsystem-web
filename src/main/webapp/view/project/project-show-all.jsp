<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Projects</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  class="show-all">
<h3>All existing projects:</h3>
<c:if test="${not empty allProjects}">

    <table class="zui-table">
        <thead>
        <tr>
            <th>Project name</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${allProjects}" var="project">
            <tr>
                <td><c:out value="${project.name}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/project/projectDetails?id=${project.id}" class="button" role="button" tabindex="0">Details</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</c:if>
</div>
</body>
</html>