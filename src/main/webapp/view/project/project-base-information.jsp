<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Projects Information</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
<h3>Base projects information:</h3>
<c:if test="${not empty projectInfos}">

    <table class="zui-table">
        <thead>
        <tr>
            <th>Project name</th>
            <th>Start project date</th>
            <th>Developers involved</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${projectInfos}" var="project">
            <tr>
                <td><c:out value="${project.projectName}"/></td>
                <td><c:out value="${project.startDate}"/></td>
                <td><c:out value="${project.developersInvolved}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/project/projectDevelopers?id=${project.id}&name=${project.projectName}" class="button" role="button" tabindex="0">Developers</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</c:if>
</div>
</body>
</html>