<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Developers in project</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
<h3>All developers working in the project:</h3>
<c:if test="${not empty developers}">

    <table class="zui-table">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Email</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${developers}" var="developer">
            <tr>
                <td><c:out value="${developer.firstName}"/></td>
                <td><c:out value="${developer.lastName}"/></td>
                <td><c:out value="${developer.age}"/></td>
                <td><c:out value="${developer.gender}"/></td>
                <td><c:out value="${developer.email}"/></td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</c:if>
</div>
</body>
</html>