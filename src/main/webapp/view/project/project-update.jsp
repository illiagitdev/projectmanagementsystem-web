<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Project Skill</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="projectUpdate">
        <h1>Update project form</h1>

        <input hidden  name="id" id="id" value="${project.id}"/>

        <label>Name
            <span class="small">Update name</span>
        </label>
        <input type="text" name="name" id="name" value="${project.name}" />


        <label>Project start date
            <span class="small">${project.projectStart}</span>
        </label>
        <input type="date" name="projectStart" id="projectStart"/>


        <label>Project cost
            <span class="small">Update project cost</span>
        </label>
        <input type="number" name="cost" id="cost"  value="${project.cost}"/>


        <label>Release date
            <span class="small">${project.releaseDate}</span>
        </label>
        <input type="date" name="releaseDate" id="releaseDate"/>

        <button type="submit">Update</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>