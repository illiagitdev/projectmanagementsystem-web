<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update Skill</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="skillUpdate">
        <h1>Update skill form</h1>

        <input hidden  name="id" id="id" value="${skill.id}"/>

        <label>Name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="skill" id="skill" value="${skill.skill}"/>

        <label>Level
            <span class="small">Enter skill level</span>
        </label>
        <input type="text" name="level" id="level" value="${skill.level}" />

        <button type="submit">Update</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>