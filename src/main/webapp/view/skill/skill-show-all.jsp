<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Skills</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  class="show-all">
<h3>All registered skills:</h3>
<c:if test="${not empty allSkills}">

    <table class="zui-table">
        <thead>
        <tr>
            <th>Skill name</th>
            <th>Skill level</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${allSkills}" var="skill">
            <tr>
                <td><c:out value="${skill.skill}"/></td>
                <td><c:out value="${skill.level}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/skill/skillUpdate?id=${skill.id}" class="button" role="button" tabindex="0">Update</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/skill/skillDelete?id=${skill.id}" class="button" role="button" tabindex="0">Delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</c:if>
</div>
</body>
</html>