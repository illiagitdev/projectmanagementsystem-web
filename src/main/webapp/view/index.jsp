<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Project management</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<h3>Hello to project management system</h3>
<p>
    Proceed to dropdown menu and select option you need!
</p>
</body>
</html>