<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Project Management</title>
</head>
<body>
<div class="navbar">
    <a href="${pageContext.request.contextPath}/">Home</a>
    <div class="dropdown">
        <button class="dropbtn">Company
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/company/showCompanies">Show companies</a>
            <a href="${pageContext.request.contextPath}/company/createCompany">Add company</a>
        </div>
    </div>

    <div class="dropdown">
        <button class="dropbtn">Project
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/project/showProjects">Show projects</a>
            <a href="${pageContext.request.contextPath}/project/createProject">Add project</a>
            <a href="${pageContext.request.contextPath}/project/projectsInfo">Information about projects</a>
            <a href="${pageContext.request.contextPath}/project/projectsExpenses">Expenses on salary</a>
        </div>
    </div>

    <div class="dropdown">
        <button class="dropbtn">Developer
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/developer/showDevelopers">Show developers</a>
            <a href="${pageContext.request.contextPath}/developer/createDeveloper">Add developer</a>
            <a href="${pageContext.request.contextPath}/developer/showBySkillDevelopers">Developers by skill</a>
            <a href="${pageContext.request.contextPath}/developer/showBySkillLevelDevelopers">Developers by skill level</a>
        </div>
    </div>

    <div class="dropdown">
        <button class="dropbtn">Customer
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/customer/showCustomers">Show customers</a>
            <a href="${pageContext.request.contextPath}/customer/createCustomer">Add customer</a>
        </div>
    </div>

    <div class="dropdown">
        <button class="dropbtn">Skill
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/skill/showSkills">Show skills</a>
            <a href="${pageContext.request.contextPath}/skill/createSkill">Add skill</a>
        </div>
    </div>

    <a href="${pageContext.request.contextPath}/about">About</a>
</div>
</body>
</html>