<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Companies</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
<h3>All registered companies:</h3>

    <c:if test="${not empty allCompanies}">

        <table class="zui-table">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${allCompanies}" var="company">
                <tr>
                    <td><c:out value="${company.name}"/></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/company/companyDetails?id=${company.id}" class="button" role="button" tabindex="0">Details</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </c:if>
</div>
</body>
</html>