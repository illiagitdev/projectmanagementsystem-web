<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Company</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createCompany">
        <h1>Create company form</h1>

        <label>Name
            <span class="small">Add name</span>
        </label>
        <input type="text" name="name" id="name" />

        <label>Location
            <span class="small">Add location</span>
        </label>
        <input type="text" name="location" id="location" />

        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>

</div>
</body>
</html>