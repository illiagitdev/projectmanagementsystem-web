<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Company</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
    <c:choose>
    <c:when test="${not empty company}">
    <h1>Company details</h1>
    <table class="zui-table">
        <tbody>
        <tr>
            <td>Company name:</td>
            <td>${company.name}</td>
        </tr>
        <tr>
            <td>Company location:</td>
            <td>${company.location}</td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/company/companyUpdate?id=${company.id}" class="button" role="button" tabindex="0">Update</a>
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/company/companyDelete?id=${company.id}" class="button" role="button" tabindex="0">Delete</a>
            </td>
        </tr>
        </tbody>
    </table>
    </c:when>
    <c:otherwise>
    <h1>Company details not available at the moment</h1>
    </c:otherwise>
    </c:choose>
    <div/>
</body>
</html>