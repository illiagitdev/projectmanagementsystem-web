<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update Customer</title>
    <style><%@include file="/view/css/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>

<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="customerUpdate">
        <h1>Update Customer form</h1>

        <input hidden  name="id" id="id" value="${customer.id}"/>

        <label>Name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="name" id="name" value="${customer.name}"/>

        <label>Level
            <span class="small">Enter budget</span>
        </label>
        <input type="text" name="budget" id="budget" value="${customer.budget}" />

        <button type="submit">Update</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error.field} ${error.error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>

</div>
</body>
</html>