<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Customer</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all">
    <c:choose>
        <c:when test="${not empty customer}">
            <h1>Customer details</h1>
            <table class="zui-table">
                <tbody>
                <tr>
                    <td>Customer name:</td>
                    <td>${customer.name}</td>
                </tr>
                <tr>
                    <td>Customer budget:</td>
                    <td>${customer.budget}</td>
                </tr>
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/customer/customerUpdate?id=${customer.id}" class="button" role="button" tabindex="0">Update</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/customer/customerDelete?id=${customer.id}" class="button" role="button" tabindex="0">Delete</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <h1>Customer details not available at the moment</h1>
        </c:otherwise>
    </c:choose>
<div/>
</body>
</html>