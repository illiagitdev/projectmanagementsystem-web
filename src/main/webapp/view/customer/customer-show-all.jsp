<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Customers</title>
    <style>
        <%@include file="/view/css/style.css"%>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  class="show-all">
<h3>All known customers:</h3>
<c:if test="${not empty allCustomers}">

    <table class="zui-table">
        <thead>
        <tr>
            <th>Customer name</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${allCustomers}" var="customer">
            <tr>
                <td><c:out value="${customer.name}"/></td>
                <td>
                    <a href="${pageContext.request.contextPath}/customer/customerDetails?id=${customer.id}" class="button" role="button" tabindex="0">Details</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</c:if>
</div>
</body>
</html>