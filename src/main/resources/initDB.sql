create schema if not exists public;

create database go_it_homework with owner postgres;

comment on schema public is 'standard public schema';

alter schema public owner to postgres;

CREATE TABLE developers (
  id SERIAL,
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  age INT NOT NULL,
  email VARCHAR(50),
  sex VARCHAR(10),
  PRIMARY KEY (id),
  CHECK (age > 0)
);

alter table developers owner to postgres;

CREATE TABLE skills (
  id SERIAL,
  skill VARCHAR(20),
  level VARCHAR(20),
  PRIMARY KEY (id)
);

alter table skills owner to postgres;

CREATE TABLE developers_skills (
  developer_id INT,
  skill_id INT,
  PRIMARY KEY (developer_id, skill_id),
  FOREIGN KEY (developer_id)
      REFERENCES developers (id),
  FOREIGN KEY (skill_id)
      REFERENCES skills (id)
);

alter table developers_skills owner to postgres;

CREATE TABLE projects (
  id SERIAL,
  name VARCHAR(50),
  release_date TIMESTAMP,
  PRIMARY KEY (id)
);

alter table projects owner to postgres;

CREATE TABLE developers_projects (
  developer_id INT,
  project_id INT,
  PRIMARY KEY (developer_id, project_id),
  FOREIGN KEY (developer_id)
      REFERENCES developers (id),
  FOREIGN KEY (project_id)
      REFERENCES projects (id)
);

alter table developers_projects owner to postgres;

CREATE TABLE companies (
  id SERIAL,
  name VARCHAR(50),
  location VARCHAR(50),
  PRIMARY KEY (id)
);

alter table companies owner to postgres;

CREATE TABLE companies_projects (
  company_id INT,
  project_id INT,
  PRIMARY KEY (company_id, project_id),
  FOREIGN KEY (company_id)
      REFERENCES companies (id),
  FOREIGN KEY (project_id)
      REFERENCES projects (id)
);

alter table companies_projects owner to postgres;

CREATE TABLE developers_companies (
  developer_id INT,
  company_id INT,
  PRIMARY KEY (developer_id, company_id),
  FOREIGN KEY (developer_id)
      REFERENCES developers (id),
  FOREIGN KEY (company_id)
      REFERENCES companies (id)
);

alter table developers_companies owner to postgres;

CREATE TABLE customers (
  id SERIAL,
  name VARCHAR(50),
  budget INT,
  PRIMARY KEY (id)
);

alter table customers owner to postgres;

CREATE TABLE customers_projects (
  customer_id INT,
  project_id INT,
  PRIMARY KEY (customer_id, project_id),
  FOREIGN KEY (customer_id)
      REFERENCES customers (id),
  FOREIGN KEY (project_id)
      REFERENCES projects (id)
);

alter table customers_projects owner to postgres;

--ALTER projects due to task2
alter table developers add column salary int;

alter table projects add column cost int;

--ALTER projects due to task with JDBC
ALTER TABLE projects add column project_start TIMESTAMP;
