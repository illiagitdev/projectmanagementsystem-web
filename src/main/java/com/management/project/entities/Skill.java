package com.management.project.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "skills")
public class Skill extends BaseEntity implements Serializable {
    private String skill;
    private String level;
    private List<Developer> developers = new ArrayList<>();

    public Skill() {
    }

    @NotEmpty
    @Column(name = "skill")
    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @ManyToMany
    @JoinTable(
            name = "developers_skills",
            joinColumns = @JoinColumn(name = "skill_id"),
            inverseJoinColumns = @JoinColumn(name = "developer_id"))
    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Skill skill1 = (Skill) o;
        return Objects.equals(skill, skill1.skill) &&
                Objects.equals(level, skill1.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), skill, level);
    }
}
