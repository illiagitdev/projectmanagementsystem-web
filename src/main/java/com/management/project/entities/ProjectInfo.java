package com.management.project.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class ProjectInfo implements Serializable {
    private int id;
    private Timestamp startDate;
    private String projectName;
    private int developersInvolved;

    public ProjectInfo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getDevelopersInvolved() {
        return developersInvolved;
    }

    public void setDevelopersInvolved(int developersInvolved) {
        this.developersInvolved = developersInvolved;
    }
}
