package com.management.project.entities;

import java.util.Arrays;
import java.util.Optional;

public enum Gender {
    MALE("MALE"),
    FEMALE("FEMALE"),
    OTHER("OTHER");

    private String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    String getGender(){
        return gender;
    }

    public static Optional<Gender> getGenderValue(String value) {
        return Arrays.stream(Gender.values())
                .filter(enumValue -> enumValue.getGender().equals(value))
                .findAny();
    }
}
