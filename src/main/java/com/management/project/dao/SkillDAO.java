package com.management.project.dao;

import com.management.project.entities.Developer;
import com.management.project.entities.Skill;

import java.util.List;

public interface SkillDAO extends DataAccessObject<Skill> {

    List<Developer> getDeveloperBySkill(String skill);
    List<Developer> getDevelopersBySkillLevel(String skillLevel);
    List<Skill> get(String skill);
    List<String> getSkillsList();

    List<String> getSkillLevelList();
}
