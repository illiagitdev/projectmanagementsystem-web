package com.management.project.dao;

import com.management.project.entities.Developer;
import com.management.project.entities.Project;
import com.management.project.entities.ProjectInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectDAOImpl implements ProjectDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(ProjectDAOImpl.class);
    private SessionFactory sessionFactory;

    public ProjectDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Map<String, Integer> getSalaryByProject() {
        try (final Session session = sessionFactory.openSession()){
            final List<Project> projects = session.createQuery("from Project ", Project.class).getResultList();
            Map<String, Integer> details =new HashMap<>();

            for (Project pr : projects) {
                String name = pr.getName();
                int totalSalary = pr.getDevelopers().stream().mapToInt(Developer::getSalary).reduce(0, Integer::sum);
                details.put(name, totalSalary);
            }
            return details;
        } catch (Exception e) {
            LOG.error("Error retrieving projects information", e);
        }
        return null;
    }

    @Override
    public List<ProjectInfo> getProjects() {
        try (final Session session = sessionFactory.openSession()){
            final List<Project> projects = session.createQuery("from Project ", Project.class).getResultList();
            List<ProjectInfo> projectInfos = new ArrayList<>();

            ProjectInfo result;
            for (Project proj : projects) {
                result = new ProjectInfo();
                result.setId(proj.getId());
                result.setProjectName(proj.getName());
                result.setStartDate(proj.getProjectStart());
                result.setDevelopersInvolved(proj.getDevelopers().size());
                projectInfos.add(result);
            }
            return projectInfos;
        } catch (Exception e) {
            LOG.error("Error retrieving projects information", e);
        }
        return null;
    }

    @Override
    public List<Developer> getDevelopersInProject(int id) {
        try (final Session session = sessionFactory.openSession()){
            Project project = session.createQuery("from Project p where p.id=:id", Project.class)
                    .setParameter("id", id)
                    .getSingleResult();
            return project.getDevelopers();
        } catch (Exception e) {
            LOG.error(String.format("Error extracting developers from project: %s", id), e);
        }
        return null;
    }

    @Override
    public List<Project> get(String name) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("FROM Project p where p.name=:name", Project.class)
                    .setParameter("name", name)
                    .getResultList();
        } catch (Exception e) {
            LOG.error(String.format("Error retrieving projects by name: %s", name), e);
        }
        return null;
    }

    @Override
    public void create(Project project) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.save(project);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error creating project with name: %s", project.getName()), e);
        }
    }

    @Override
    public void update(Project project) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.update(project);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error updating project(%s): %s", project.getId(), project.getName()));
        }
    }

    @Override
    public Project get(int id) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("FROM Project p where p.id=:id", Project.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            LOG.error(String.format("Error retrieving projects by id=%s", id), e);
        }
        return null;
    }

    @Override
    public List<Project> getAll() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Project ", Project.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error extracting projects", e);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.createQuery("delete Project as p where p.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Project with id=%s", id), e);
        }
    }
}
