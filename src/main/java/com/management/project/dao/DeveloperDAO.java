package com.management.project.dao;

import com.management.project.entities.Developer;

import java.util.List;

public interface DeveloperDAO extends DataAccessObject<Developer> {
    List<Developer> get(String email);
}
