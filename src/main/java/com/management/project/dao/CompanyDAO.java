package com.management.project.dao;

import com.management.project.entities.Company;

import java.util.List;

public interface CompanyDAO extends DataAccessObject<Company> {
    List<Company> get(String name);
}
