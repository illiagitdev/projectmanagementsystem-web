package com.management.project.dao;

import com.management.project.entities.Company;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class CompanyDAOImpl implements CompanyDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(CompanyDAOImpl.class);
    private SessionFactory sessionFactory;

    public CompanyDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Company company) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.save(company);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error saving company: %s, %s", company.getName(), company.getLocation()));
        }
    }

    @Override
    public void update(Company company) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.update(company);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error updating company(%s),: %s", company.getId(), company.getName()));
        }
    }

    @Override
    public Company get(int id) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Company c where c.id=:id", Company.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            LOG.error(String.format("Error extracting company with id =%s", id), e);
        }
        return null;
    }

    @Override
    public List<Company> getAll() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Company ", Company.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error extracting companies", e);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.createQuery("delete Company as c where c.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Company with id=%s", id), e);
        }
    }

    @Override
    public List<Company> get(String name) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Company c where c.name=:name", Company.class)
                    .setParameter("name", name)
                    .getResultList();
        } catch (Exception e) {
            LOG.error(String.format("extracting company with name: %s", name), e);
        }
        return null;
    }
}
