package com.management.project.dao;

import com.management.project.entities.Developer;
import com.management.project.entities.Skill;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class SkillDAOImpl implements SkillDAO {
    private static final Logger LOG = LogManager.getLogger(SkillDAOImpl.class);
    private SessionFactory sessionFactory;

    public SkillDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Developer> getDeveloperBySkill(String skill) {
        try (final Session session = sessionFactory.openSession()){
            List<Skill> skillList = session.createQuery("from Skill s where s.skill=:skill", Skill.class)
                    .setParameter("skill", skill)
                    .getResultList();
            List<Developer> developers = skillList.stream().map(Skill::getDevelopers).flatMap(List::stream).collect(Collectors.toList());
            LOG.debug(String.format("developers with skill: %s - %s", skill, developers.isEmpty()));
            return developers;
        } catch (Exception e) {
            LOG.error(String.format("Error retrieving developers with skill: %s", skill), e);
        }
        return null;
    }

    @Override
    public List<Developer> getDevelopersBySkillLevel(String skillLevel) {
        try (final Session session = sessionFactory.openSession()){
            List<Skill> skillList = session.createQuery("from Skill s where s.level=:level", Skill.class)
                    .setParameter("level", skillLevel)
                    .getResultList();
            List<Developer> developers = skillList.stream().map(Skill::getDevelopers).flatMap(List::stream).collect(Collectors.toList());
            LOG.debug(String.format("developers with skillLevel: %s - %s", skillLevel, developers.isEmpty()));
            return developers;
        } catch (Exception e) {
            LOG.error(String.format("Error retrieving developers with skillLevel: %s", skillLevel), e);
        }
        return null;
    }

    @Override
    public List<Skill> get(String skill) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Skill s where s.skill=:skill", Skill.class)
                    .setParameter("skill", skill)
                    .getResultList();
        } catch (Exception e) {
            LOG.error(String.format("error retrieving skill: %s", skill), e);
        }
        return null;
    }

    @Override
    public List<String> getSkillsList() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("select distinct s.skill from Skill s ", String.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error retrieving skills list", e);
        }
        return null;
    }

    @Override
    public List<String> getSkillLevelList() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("select distinct s.level from Skill s ", String.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error retrieving skills levels list", e);
        }
        return null;
    }

    @Override
    public void create(Skill skill) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.save(skill);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("error creating skill: %s - %s", skill.getSkill(), skill.getLevel()));
        }
    }

    @Override
    public void update(Skill skill) {
        Transaction transaction = null;
        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.update(skill);
            transaction.commit();
        } catch (Exception e) {
            if ((transaction != null)) {
                transaction.rollback();
            }
            LOG.error(String.format("Error updating skill(%s): %s", skill.getId(), skill.getSkill()));
        }
    }

    @Override
    public Skill get(int id) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Skill s where s.id=:id", Skill.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            LOG.error("Error extracting skills", e);
        }
        return null;
    }

    @Override
    public List<Skill> getAll() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Skill ", Skill.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error extracting skills", e);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.createQuery("delete Skill as s where s.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Skill with id=%s", id), e);
        }
    }
}
