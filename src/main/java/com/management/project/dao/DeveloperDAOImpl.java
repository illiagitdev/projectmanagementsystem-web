package com.management.project.dao;

import com.management.project.entities.Developer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class DeveloperDAOImpl implements DeveloperDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(DeveloperDAOImpl.class);
    private SessionFactory sessionFactory;

    public DeveloperDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Developer developer) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.save(developer);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error creating developer with parameters: %s, %s, %s",
                    developer.getFirstName(), developer.getLastName(), developer.getEmail()), e);
        }
    }

    @Override
    public void update(Developer developer) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.update(developer);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error updating developer(%s): %s", developer.getId(), developer.getEmail()));
        }
    }

    @Override
    public Developer get(int id) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Developer d where d.id=:id", Developer.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            LOG.error(String.format("Error extracting developer with id=%s", id), e);
        }
        return null;
    }

    @Override
    public List<Developer> getAll() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Developer ", Developer.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error extracting developers", e);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.createQuery("delete Developer as d where d.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Developer with id=%s", id), e);
        }
    }

    @Override
    public List<Developer> get(String email) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Developer d where d.email=:email", Developer.class)
                    .setParameter("email", email)
                    .getResultList();
        } catch (Exception e) {
            LOG.error(String.format("Error retrieving developer by email: %s", email), e);
        }
        return null;
    }
}
