package com.management.project.dao;

import com.management.project.entities.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {
    private static final Logger LOG = LogManager.getFormatterLogger(CustomerDAOImpl.class);
    private SessionFactory sessionFactory;

    public CustomerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Customer customer) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Error creating customer %s", customer.getName()), e);
        }
    }

    @Override
    public void update(Customer customer) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        } catch (Exception e) {
            LOG.error(String.format("Error saving customer(%s: %s", customer.getId(), customer.getName()), e);
        }
    }

    @Override
    public Customer get(int id) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Customer c where c.id=:id", Customer.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            LOG.error(String.format("Error extracting customer with id: %s", id), e);
        }
        return null;
    }

    @Override
    public List<Customer> getAll() {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Customer ", Customer.class).getResultList();
        } catch (Exception e) {
            LOG.error("Error extracting customers", e);
        }
        return null;
    }

    @Override
    public void delete(int id) {
        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.createQuery("delete Customer as c where c.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.error(String.format("Customer with id=%s", id), e);
        }
    }

    @Override
    public List<Customer> get(String name) {
        try (final Session session = sessionFactory.openSession()){
            return session.createQuery("from Customer c where c.name=:name", Customer.class)
                    .setParameter("name", name)
                    .getResultList();
        } catch (Exception e) {
            LOG.error(String.format("Error extracting customer with name: %s", name), e);
        }
        return null;
    }
}
