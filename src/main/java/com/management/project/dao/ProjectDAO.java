package com.management.project.dao;

import com.management.project.entities.Developer;
import com.management.project.entities.Project;
import com.management.project.entities.ProjectInfo;

import java.util.List;
import java.util.Map;

public interface ProjectDAO extends DataAccessObject<Project> {

    Map<String, Integer> getSalaryByProject();
    List<ProjectInfo> getProjects();
    List<Developer> getDevelopersInProject(int id);

    List<Project> get(String name);
}
