package com.management.project.dao;

import com.management.project.entities.BaseEntity;

import java.util.List;

public interface DataAccessObject<T extends BaseEntity> {

    void create(T t);
    void update(T t);
    T get(int id);
    List<T> getAll();
    void delete(int id);

}
