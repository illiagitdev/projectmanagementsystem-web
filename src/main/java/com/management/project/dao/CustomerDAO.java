package com.management.project.dao;

import com.management.project.entities.Customer;

import java.util.List;

public interface CustomerDAO extends DataAccessObject<Customer> {
    List<Customer> get(String name);
}
