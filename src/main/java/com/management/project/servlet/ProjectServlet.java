package com.management.project.servlet;

import com.management.project.config.HibernateDatabaseConnector;
import com.management.project.dao.ProjectDAOImpl;
import com.management.project.entities.Developer;
import com.management.project.entities.Project;
import com.management.project.entities.ProjectInfo;
import com.management.project.exceptions.ErrorMessage;
import com.management.project.utils.Projects;
import com.management.project.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/project/*")
public class ProjectServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(ProjectServlet.class);
    private Projects service;

    @Override
    public void init() throws ServletException {
        super.init();
        service = new Projects(new ProjectDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/showProjects")) {

            final List<Project> allProjects = service.getAllCompanies();
            req.setAttribute("allProjects", allProjects);
            LOG.debug(String.format("/showProjects: allProjects=%s", Objects.nonNull(allProjects)));
            req.getRequestDispatcher("/view/project/project-show-all.jsp").forward(req, resp);

        } else if (action.startsWith("/projectDetails")) {

            int id = Utils.getId(req);
            Project project = service.getProject(id);
            req.setAttribute("project", project);
            LOG.debug(String.format("/projectDetails: idValue=%s, passParameter=%s", id, Objects.nonNull(project)));
            req.getRequestDispatcher("/view/project/project-details.jsp").forward(req, resp);

        } else if (action.startsWith("/projectUpdate")) {

            int id = Utils.getId(req);
            Project project = service.getProject(id);
            req.setAttribute("project", project);
            req.getRequestDispatcher("/view/project/project-update.jsp").forward(req, resp);

        } else if (action.startsWith("/projectDelete")) {

            int id =Utils.getId(req);
            service.deleteProject(id);
            String message = "Project deleted";
            req.setAttribute("message", message);
            LOG.debug(String.format("Project delete id: %s", id));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        } else if (action.startsWith("/createProject")) {

            LOG.debug("/createProject");
            req.getRequestDispatcher("/view/project/project-create.jsp").forward(req, resp);

        } else if (action.startsWith("/projectsInfo")) {

            List<ProjectInfo> projectInfos = service.getProjectsInfo();
            req.setAttribute("projectInfos", projectInfos);
            LOG.debug(String.format("/projectsInfo(empty?): %s", projectInfos.isEmpty()));
            req.getRequestDispatcher("/view/project/project-base-information.jsp").forward(req, resp);

        } else if (action.startsWith("/projectDevelopers")) {

            int id = Utils.getId(req);
            final List<Developer> developers = service.getProjectDevelopers(id);
            req.setAttribute("developers", developers);
            LOG.debug(String.format("/projectDevelopers: %s, count=%s", id, developers.size()));
            req.getRequestDispatcher("/view/project/project-developers-in.jsp").forward(req, resp);

        } else if (action.startsWith("/projectsExpenses")) {

            final Map<String, Integer> expensesDetails = service.getExpensesOnDevelopers();
            req.setAttribute("expensesMap", expensesDetails);
            req.getRequestDispatcher("/view/project/project-developers-salary.jsp").forward(req, resp);
            resp.getWriter().write("processing ... :)");

        } else {

            LOG.debug(String.format("%s -> no action triggered", action));
            req.getRequestDispatcher("/view/index.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/createProject")) {

            Project project = service.mapProject(req);
            List<ErrorMessage> errorMessages = validateProject(project);

            if (!errorMessages.isEmpty()) {
                req.setAttribute("errors", errorMessages);
                LOG.debug("/createProject errors" + errorMessages.stream().
                        map(el -> String.format("%s - %s", el.getField(), el.getError()))
                        .collect(Collectors.joining(", ")));
                req.getRequestDispatcher("/view/project/project-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Project created: %s", project.getName()));
                service.createProject(project);
                String message = String.format("Project %s created!", project.getName());
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/success.jsp").forward(req, resp);
            }
        } else if (action.startsWith("/projectUpdate")) {

            Project project = service.mapProject(req);
            service.updateProject(project);
            String message = "Project updated";
            req.setAttribute("message", message);
            LOG.debug(String.format("Project update: %s %s", project.getId(), project.getName()));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        }
    }

    private List<ErrorMessage> validateProject(Project project) {
        final List<ErrorMessage> errorMessages = new ArrayList<>();
        final List<Project> persistentCustomer = service.getProject(project.getName());
        if (Objects.nonNull(persistentCustomer)) {
            for (Project p : persistentCustomer) {
                if (project.getName().equals(p.getName())){
                    errorMessages.add(new ErrorMessage(p.getName(), "Project already exists"));
                }
            }
        }
        return errorMessages;
    }

    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }

}
