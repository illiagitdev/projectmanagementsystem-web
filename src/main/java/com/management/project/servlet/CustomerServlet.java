package com.management.project.servlet;

import com.management.project.config.HibernateDatabaseConnector;
import com.management.project.dao.CustomerDAOImpl;
import com.management.project.entities.Customer;
import com.management.project.exceptions.ErrorMessage;
import com.management.project.utils.Customers;
import com.management.project.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/customer/*")
public class CustomerServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(CustomerServlet.class);
    private Customers service;

    @Override
    public void init() throws ServletException {
        super.init();
        service = new Customers(new CustomerDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/showCustomers")) {

            final List<Customer> allCustomers = service.getAllCustomers();
            req.setAttribute("allCustomers", allCustomers);
            LOG.debug(String.format("/showCustomers: allCustomers=%s", Objects.nonNull(allCustomers)));
            req.getRequestDispatcher("/view/customer/customer-show-all.jsp").forward(req, resp);

        } else if (action.startsWith("/customerDetails")) {

            int id = Utils.getId(req);
            final Customer customer = service.getCustomer(id);
            req.setAttribute("customer", customer);
            LOG.debug(String.format("/customerDetails: idValue=%s, passParameter=%s", id, Objects.nonNull(customer)));
            req.getRequestDispatcher("/view/customer/customer-details.jsp").forward(req, resp);

        } else if (action.startsWith("/createCustomer")) {

            req.getRequestDispatcher("/view/customer/customer-create.jsp").forward(req, resp);

        } else if (action.startsWith("/customerUpdate")) {

            int id = Utils.getId(req);
            Customer customer = service.getCustomer(id);
            req.setAttribute("customer", customer);
            req.getRequestDispatcher("/view/customer/customer-update.jsp").forward(req, resp);

        } else if (action.startsWith("/customerDelete")) {

            int id =Utils.getId(req);
            service.deleteCustomer(id);
            String message = "Customer deleted";
            req.setAttribute("message", message);
            LOG.debug(String.format("Customer delete id: %s", id));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        } else {

            LOG.debug(String.format("%s -> no action triggered", action));
            req.getRequestDispatcher("/view/index.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/createCustomer")) {

            Customer customer = service.mapCustomer(req);
            List<ErrorMessage> errorMessages = validateCustomer(customer);

            if (!errorMessages.isEmpty()) {
                req.setAttribute("errors", errorMessages);
                LOG.debug("/createCustomer errors" + errorMessages.stream().
                        map(el -> String.format("%s - %s", el.getField(), el.getError()))
                        .collect(Collectors.joining(", ")));
                req.getRequestDispatcher("/view/customer/customer-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Customer created: %s", customer.getName()));
                service.createCustomer(customer);
                String message = String.format("Customer %s : %s $\" created!", customer.getName(), customer.getBudget());
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/success.jsp").forward(req, resp);
            }

         } else if (action.startsWith("/customerUpdate")) {

            Customer customer = service.mapCustomer(req);
            service.updateCustomer(customer);
            String message = "Customer updated";
            req.setAttribute("message", message);
            LOG.debug(String.format("Customer update: %s %s %s",
                    customer.getId(), customer.getName(), customer.getBudget()));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        }
    }

    private List<ErrorMessage> validateCustomer(Customer customer) {
        final List<ErrorMessage> errorMessages = new ArrayList<>();
        final List<Customer> persistentCustomer = service.getCustomer(customer.getName());
        if (Objects.nonNull(persistentCustomer)) {
            for (Customer c : persistentCustomer) {
                if (customer.getName().equals(c.getName())){
                    errorMessages.add(new ErrorMessage(c.getName(), "Customer already exists"));
                }
            }
        }
        return errorMessages;
    }

    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}
