package com.management.project.servlet;

import com.management.project.config.HibernateDatabaseConnector;
import com.management.project.dao.CompanyDAOImpl;
import com.management.project.entities.Company;
import com.management.project.exceptions.ErrorMessage;
import com.management.project.utils.Companies;
import com.management.project.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/company/*")
public class CompanyServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(CompanyServlet.class);
    private Companies service;

    @Override
    public void init() throws ServletException {
        super.init();
        service = new Companies(new CompanyDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/showCompanies")) {

            final List<Company> allCompanies = service.getAllCompanies();
            req.setAttribute("allCompanies", allCompanies);
            LOG.debug(String.format("/showCompanies: allCompanies=%s", Objects.nonNull(allCompanies)));
            req.getRequestDispatcher("/view/company/company-show-all.jsp").forward(req, resp);

        } else if (action.startsWith("/companyDetails")) {

            int id =  Utils.getId(req);
            final Company company = service.getCompany(id);
            req.setAttribute("company", company);
            LOG.debug(String.format("/companyDetails: idValue=%s, passParameter=%s", id, Objects.nonNull(company)));
            req.getRequestDispatcher("/view/company/company-details.jsp").forward(req, resp);

        } else if (action.startsWith("/companyUpdate")) {

            int id = Utils.getId(req);
            Company company = service.getCompany(id);
            req.setAttribute("company", company);
            req.getRequestDispatcher("/view/company/company-update.jsp").forward(req, resp);

        } else if (action.startsWith("/companyDelete")) {

            int id =Utils.getId(req);
            service.deleteCompany(id);
            String message = "Company deleted";
            req.setAttribute("message", message);
            LOG.debug(String.format("Company delete id: %s", id));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        } else if (action.startsWith("/createCompany")) {

            req.getRequestDispatcher("/view/company/company-create.jsp").forward(req, resp);

        } else {

            LOG.debug(String.format("%s -> no action triggered", action));
            req.getRequestDispatcher("/view/index.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/createCompany")) {

            Company company = service.mapCompany(req);
            List<ErrorMessage> errorMessages = validateCustomer(company);

            if (!errorMessages.isEmpty()) {
                req.setAttribute("errors", errorMessages);
                LOG.debug("/createCompany errors" + errorMessages.stream().
                        map(el -> String.format("%s - %s", el.getField(), el.getError()))
                        .collect(Collectors.joining(", ")));
                req.getRequestDispatcher("/view/company/company-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Company created: %s", company.getName()));
                service.createCompany(company);
                String message = String.format("Company %s : %s created!", company.getName(), company.getLocation());
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/success.jsp").forward(req, resp);
            }

        } else if (action.startsWith("/companyUpdate")) {

            Company company = service.mapCompany(req);
            service.updateCompany(company);
            String message = "Company updated";
            req.setAttribute("message", message);
            LOG.debug(String.format("Company update: %s %s %s", company.getId(), company.getName(), company.getLocation()));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        }
    }

    private List<ErrorMessage> validateCustomer(Company customer) {
        final List<ErrorMessage> errorMessages = new ArrayList<>();
        final List<Company> persistentCompany = service.getCcompany(customer.getName());
        if (Objects.nonNull(persistentCompany)) {
            for (Company c : persistentCompany) {
                if (customer.getName().equals(c.getName())){
                    errorMessages.add(new ErrorMessage(c.getName(), "Company already exists"));
                }
            }
        }
        return errorMessages;
    }

    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}
