package com.management.project.servlet;

import com.management.project.config.HibernateDatabaseConnector;
import com.management.project.dao.DeveloperDAOImpl;
import com.management.project.dao.SkillDAOImpl;
import com.management.project.entities.Developer;
import com.management.project.entities.Gender;
import com.management.project.exceptions.ErrorMessage;
import com.management.project.utils.Developers;
import com.management.project.utils.Skills;
import com.management.project.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/developer/*")
public class DeveloperServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(DeveloperServlet.class);
    private Developers service;
    private Skills skillService;

    @Override
    public void init() throws ServletException {
        super.init();
        service = new Developers(new DeveloperDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
        skillService = new Skills(new SkillDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/showDevelopers")) {

            List<Developer> developers = service.getAllDevelopers();
            String message = "All available developers:";
            req.setAttribute("message", message);
            req.setAttribute("developers", developers);
            LOG.debug(String.format("/showDevelopers: developers=%s", Objects.nonNull(developers)));
            req.getRequestDispatcher("/view/developer/developers-show.jsp").forward(req, resp);

        } else if (action.startsWith("/developerDetails")) {

            int id = Utils.getId(req);
            Developer developer = service.getDeveloper(id);
            req.setAttribute("developer", developer);
            LOG.debug(String.format("/developerDetails: idValue=%s, passParameter=%s", id, Objects.nonNull(developer)));
            req.getRequestDispatcher("/view/developer/developers-details.jsp").forward(req, resp);

        } else if (action.startsWith("/developerUpdate")) {

            int id = Utils.getId(req);
            Developer developer = service.getDeveloper(id);
            req.setAttribute("developer", developer);
            req.setAttribute("gender", Gender.values());
            req.getRequestDispatcher("/view/developer/developer-update.jsp").forward(req, resp);

        } else if (action.startsWith("/developerDelete")) {

            int id =Utils.getId(req);
            service.deleteDeveloper(id);
            String message = "Developer deleted";
            req.setAttribute("message", message);
            LOG.debug(String.format("Developer delete id: %s", id));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        } else if (action.startsWith("/createDeveloper")) {

            req.setAttribute("gender", Gender.values());
            req.getRequestDispatcher("/view/developer/developer-create.jsp").forward(req, resp);

        } else if (action.startsWith("/showBySkillDevelopers")) {

            String skill = req.getParameter("skill");
            List<String> skillList= skillService.getSkillsList();
            List<Developer> developers = skillService.getDevelopersWithSkill(skill);;
            String message = "Developers by specified skill:";

            req.setAttribute("skillList", skillList);
            req.setAttribute("message", message);
            req.setAttribute("developers", developers);
            LOG.debug(String.format("/showBySkillDevelopers: skill=%s, skillList=%s, developers=%s",
                    skill, Objects.nonNull(skillList), Objects.nonNull(developers)));
            req.getRequestDispatcher("/view/developer/developers-skill-show.jsp").forward(req, resp);

        } else if (action.startsWith("/showBySkillLevelDevelopers")) {

            String skillLevel = req.getParameter("skillLevel");
            List<String> skillLevelList= skillService.getSkillLevelList();
            List<Developer> developers = skillService.getDevelopersWithSkillLevel(skillLevel);
            String message = "Developers by specified skill level:";

            req.setAttribute("skillLevelList", skillLevelList);
            req.setAttribute("message", message);
            req.setAttribute("developers", developers);
            LOG.debug(String.format("/showBySkillLevelDevelopers: skillLevel=%s, skillLevelList=%s, developers=%s",
                    skillLevel, Objects.nonNull(skillLevelList), Objects.nonNull(developers)));
            req.getRequestDispatcher("/view/developer/developers-skillLevel-show.jsp").forward(req, resp);

        } else {

            LOG.debug(String.format("%s -> no action triggered", action));
            req.getRequestDispatcher("/view/index.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/createDeveloper")) {

            Developer developer = service.mapDeveloper(req);
            List<ErrorMessage> errorMessages = validateCustomer(developer);

            if (!errorMessages.isEmpty()) {
                req.setAttribute("errors", errorMessages);
                LOG.debug("/createDeveloper errors" + errorMessages.stream().
                        map(el -> String.format("%s - %s", el.getField(), el.getError()))
                        .collect(Collectors.joining(", ")));
                req.getRequestDispatcher("/view/developer/developer-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Developer created: %s, %s, %s",
                        developer.getFirstName(), developer.getLastName(), developer.getEmail()));
                service.createDeveloper(developer);
                String message = String.format("Developer: '%s' '%s' '%s' - created!",
                        developer.getFirstName(), developer.getLastName(), developer.getEmail());
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/success.jsp").forward(req, resp);
            }

        } else if (action.startsWith("/developerUpdate")) {

            Developer developer = service.mapDeveloper(req);
            service.updateDeveloper(developer);
            String message = "Developer updated";
            req.setAttribute("message", message);
            LOG.debug(String.format("Developer update: %s %s %s %s", developer.getId(), developer.getFirstName(),
                    developer.getLastName(), developer.getEmail()));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        }
    }

    private List<ErrorMessage> validateCustomer(Developer developer) {
        final List<ErrorMessage> errorMessages = new ArrayList<>();
        final List<Developer> persistentDeveloper = service.getDeveloper(developer.getEmail());
        if (Objects.nonNull(persistentDeveloper)) {
            for (Developer d: persistentDeveloper) {
                if (developer.getEmail().equals(d.getEmail())){
                    errorMessages.add(new ErrorMessage(d.getEmail(), "Developer with this email already exists"));
                }
                if(developer.getFirstName().equals(d.getFirstName()) && developer.getLastName().equals(d.getLastName())) {
                    errorMessages.add(new ErrorMessage(d.getFirstName(), "Developer with same first name"));
                    errorMessages.add(new ErrorMessage(d.getLastName(), "Developer with same last name"));
                }
            }
        }
        return errorMessages;
    }

    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}
