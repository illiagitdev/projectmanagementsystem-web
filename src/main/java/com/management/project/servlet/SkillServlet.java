package com.management.project.servlet;

import com.management.project.config.HibernateDatabaseConnector;
import com.management.project.dao.SkillDAOImpl;
import com.management.project.entities.Skill;
import com.management.project.exceptions.ErrorMessage;
import com.management.project.utils.Skills;
import com.management.project.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/skill/*")
public class SkillServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(SkillServlet.class);
    private Skills service;

    @Override
    public void init() throws ServletException {
        super.init();
        service = new Skills(new SkillDAOImpl(HibernateDatabaseConnector.getSessionFactory()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/showSkills")) {

            final List<Skill> allSkills = service.getAllSkills();
            req.setAttribute("allSkills", allSkills);
            LOG.debug(String.format("/showSkills: allSkills=%s", Objects.nonNull(allSkills)));
            req.getRequestDispatcher("/view/skill/skill-show-all.jsp").forward(req, resp);

        } else if (action.startsWith("/createSkill")) {

            req.getRequestDispatcher("/view/skill/skill-create.jsp").forward(req, resp);

        } else if (action.startsWith("/skillUpdate")) {

            int id = Utils.getId(req);
            Skill skill = service.getSkill(id);
            req.setAttribute("skill", skill);
            req.getRequestDispatcher("/view/skill/skill-update.jsp").forward(req, resp);

        } else if (action.startsWith("/skillDelete")) {

            int id =Utils.getId(req);
            service.deleteSkill(id);
            String message = "Skill deleted";
            req.setAttribute("message", message);
            LOG.debug(String.format("Skill delete id: %s", id));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        } else {

            LOG.debug(String.format("%s -> no action triggered", action));
            req.getRequestDispatcher("/view/index.jsp").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);

        if (action.startsWith("/createSkill")) {

            Skill skill = service.mapSkill(req);
            List<ErrorMessage> errorMessages = validateSkill(skill);

            if (!errorMessages.isEmpty()) {
                req.setAttribute("errors", errorMessages);
                LOG.debug("/createSkill errors" + errorMessages.stream().
                        map(el -> String.format("%s - %s", el.getField(), el.getError()))
                        .collect(Collectors.joining(", ")));
                req.getRequestDispatcher("/view/skill/skill-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Skill created: %s", skill.getSkill()));
                service.createSkill(skill);
                String message = String.format("Skill %s : %s created!", skill.getSkill(), skill.getLevel());
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/success.jsp").forward(req, resp);
            }

        } else if (action.startsWith("/skillUpdate")) {

            Skill skill = service.mapSkill(req);
            service.updateSkill(skill);
            String message = "Skill updated";
            req.setAttribute("message", message);
            LOG.debug(String.format("Skill update: %s %s %s", skill.getId(), skill.getSkill(), skill.getLevel()));
            req.getRequestDispatcher("/view/success.jsp").forward(req, resp);

        }
    }

    private List<ErrorMessage> validateSkill(Skill skill) {
        final List<ErrorMessage> errorMessages = new ArrayList<>();
        final List<Skill> persistentSkill = service.getSkillLevel(skill.getSkill());
        if (Objects.nonNull(persistentSkill)) {
            for (Skill s : persistentSkill) {
                if (skill.getSkill().equals(s.getSkill()) && skill.getLevel().equals(s.getLevel())){
                    errorMessages.add(new ErrorMessage(s.getSkill(), "Skill already exists"));
                    errorMessages.add(new ErrorMessage(s.getLevel(), String.format("Skill %s with such level exists", s.getLevel())));
                }
            }
        }
        return errorMessages;
    }

    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}
