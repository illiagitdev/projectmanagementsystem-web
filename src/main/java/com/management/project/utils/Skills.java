package com.management.project.utils;

import com.management.project.dao.SkillDAO;
import com.management.project.entities.Developer;
import com.management.project.entities.Skill;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class Skills {
    private SkillDAO skillDAO;

    public Skills(SkillDAO skillDAO) {
        this.skillDAO = skillDAO;
    }

    public List<Skill> getAllSkills() {
        return skillDAO.getAll();
    }

    public List<Skill> getSkillLevel(String skill) {
        return skillDAO.get(skill);
    }

    public void createSkill(Skill skill) {
        skillDAO.create(skill);
    }

    public List<String> getSkillsList() {
        return skillDAO.getSkillsList();
    }

    public List<Developer> getDevelopersWithSkill(String skill) {
        return skillDAO.getDeveloperBySkill(skill);
    }

    public List<Developer> getDevelopersWithSkillLevel(String skillLevel) {
        return skillDAO.getDevelopersBySkillLevel(skillLevel);
    }

    public List<String> getSkillLevelList() {
        return skillDAO.getSkillLevelList();
    }

    public Skill getSkill(int id) {
        return skillDAO.get(id);
    }

    public Skill mapSkill(HttpServletRequest req) {
        String id = req.getParameter("id");
        String skill = req.getParameter("skill").trim();
        String level = req.getParameter("level").trim();
        Skill newSkill = new Skill();
        newSkill.setSkill(skill);
        newSkill.setLevel(level);
        if(id != null) {
            newSkill.setId(Integer.parseInt(id));
        }
        return newSkill;
    }

    public void updateSkill(Skill skill) {
        skillDAO.update(skill);
    }

    public void deleteSkill(int id) {
        skillDAO.delete(id);
    }
}
