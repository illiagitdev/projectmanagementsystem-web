package com.management.project.utils;

import com.management.project.dao.DeveloperDAO;
import com.management.project.entities.Developer;
import com.management.project.entities.Gender;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public class Developers {
    private DeveloperDAO developerDAO;

    public Developers(DeveloperDAO developerDAO) {
        this.developerDAO = developerDAO;
    }

    public List<Developer> getAllDevelopers() {
        return developerDAO.getAll();
    }

    public List<Developer> getDeveloper(String email) {
        return developerDAO.get(email);
    }

    public void createDeveloper(Developer developer) {
        developerDAO.create(developer);
    }

    public Developer getDeveloper(int id) {
        return developerDAO.get(id);
    }

    public Developer mapDeveloper(HttpServletRequest req) {
        String id = req.getParameter("id");
        String firstName = req.getParameter("firstName").trim();
        String lastName = req.getParameter("lastName").trim();
        String age = req.getParameter("age").trim();
        String email = req.getParameter("email").trim();
        String gender = req.getParameter("gender").trim();
        String salary = req.getParameter("salary").trim();
        Optional<Gender> genderValue = Gender.getGenderValue(gender);
        Developer developer = new Developer();
        developer.setFirstName(firstName);
        developer.setLastName(lastName);
        developer.setAge(Integer.parseInt(age));
        developer.setEmail(email);
        developer.setGender(genderValue.get());
        developer.setSalary(Integer.parseInt(salary));
        if(id != null) {
            developer.setId(Integer.parseInt(id));
        }
        return developer;
    }

    public void updateDeveloper(Developer developer) {
        developerDAO.update(developer);
    }

    public void deleteDeveloper(int id) {
        developerDAO.delete(id);
    }
}
