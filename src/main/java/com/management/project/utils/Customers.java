package com.management.project.utils;

import com.management.project.dao.CustomerDAO;
import com.management.project.entities.Customer;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class Customers {
    private CustomerDAO customerDAO;

    public Customers(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    public List<Customer> getAllCustomers() {
        return customerDAO.getAll();
    }

    public List<Customer> getCustomer(String name) {
        return customerDAO.get(name);
    }

    public void createCustomer(Customer customer) {
        customerDAO.create(customer);
    }

    public Customer getCustomer(int id) {
        return customerDAO.get(id);
    }

    public Customer mapCustomer(HttpServletRequest req) {
        String id = req.getParameter("id");
        String name = req.getParameter("name").trim();
        String budget = req.getParameter("budget").trim();
        Customer customer = new Customer();
        customer.setName(name);
        customer.setBudget(Integer.parseInt(budget));
        if(id != null) {
            customer.setId(Integer.parseInt(id));
        }
        return customer;
    }

    public void updateCustomer(Customer customer) {
        customerDAO.update(customer);
    }

    public void deleteCustomer(int id) {
        customerDAO.delete(id);
    }
}
