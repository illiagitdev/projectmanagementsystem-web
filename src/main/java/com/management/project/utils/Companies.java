package com.management.project.utils;

import com.management.project.dao.CompanyDAO;
import com.management.project.entities.Company;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class Companies {
    private CompanyDAO companyDAO;

    public Companies(CompanyDAO companyDAO) {
        this.companyDAO = companyDAO;
    }

    public List<Company> getAllCompanies() {
        return companyDAO.getAll();
    }

    public List<Company> getCcompany(String name) {
        return companyDAO.get(name);
    }

    public void createCompany(Company company) {
        companyDAO.create(company);
    }

    public Company getCompany(int id) {
        return companyDAO.get(id);
    }

    public Company mapCompany(HttpServletRequest req) {
        String id = req.getParameter("id");
        String name = req.getParameter("name").trim();
        String location = req.getParameter("location").trim();
        Company company = new Company();
        company.setName(name);
        company.setLocation(location);
        if(id != null) {
            company.setId(Integer.parseInt(id));
        }
        return company;
    }

    public void updateCompany(Company company) {
        companyDAO.update(company);
    }

    public void deleteCompany(int id) {
        companyDAO.delete(id);
    }
}
