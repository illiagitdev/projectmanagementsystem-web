package com.management.project.utils;

import com.management.project.dao.ProjectDAO;
import com.management.project.entities.Developer;
import com.management.project.entities.Project;
import com.management.project.entities.ProjectInfo;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Projects {
    private ProjectDAO projectDAO;

    public Projects(ProjectDAO projectDAO) {
        this.projectDAO = projectDAO;
    }

    public List<Project> getAllCompanies() {
        return projectDAO.getAll();
    }

    public List<Project> getProject(String name) {
        return projectDAO.get(name);
    }

    public void createProject(Project project) {
        projectDAO.create(project);
    }

    public List<ProjectInfo> getProjectsInfo() {
        return projectDAO.getProjects();
    }

    public List<Developer> getProjectDevelopers(int id) {
        return projectDAO.getDevelopersInProject(id);
    }

    public Project getProject(int id) {
        return projectDAO.get(id);
    }

    public Map<String, Integer> getExpensesOnDevelopers() {
        return projectDAO.getSalaryByProject();
    }

    public Project mapProject(HttpServletRequest req) {
        String id = req.getParameter("id");
        String name = req.getParameter("name").trim();
        String projectStart = req.getParameter("projectStart").trim();
        String cost = req.getParameter("cost").trim();
        String releaseDate = req.getParameter("releaseDate").trim();
        Project project = new Project();
        project.setName(name);
        project.setProjectStart(validateDate(projectStart));
        project.setCost(Integer.parseInt(cost));
        project.setReleaseDate(validateDate(releaseDate));
        if(id != null) {
            project.setId(Integer.parseInt(id));
        }
        return project;
    }

    private static Timestamp validateDate(String value) {
        if (value.isEmpty()) {
            return null;
        }
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");
            Date parseDate = formatter.parse(value);
            return new Timestamp(parseDate.getTime());
        } catch (Exception e) {
            System.err.println(String.format("validateDate:error with value '%s'", e));
        }
        return null;
    }

    public void updateProject(Project project) {
        projectDAO.update(project);
    }

    public void deleteProject(int id) {
        projectDAO.delete(id);
    }
}
