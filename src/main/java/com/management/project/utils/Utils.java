package com.management.project.utils;

import javax.servlet.http.HttpServletRequest;

public class Utils {
    public static int getId(HttpServletRequest req) {
        String idValue = req.getParameter("id");
        int id = Integer.parseInt(idValue);
        return id;
    }
}
